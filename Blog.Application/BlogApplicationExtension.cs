﻿using Blog.Application.Blogs;
using Blog.Application.Contract.Blogs;
using Blog.Application.Contract.User;
using Blog.Application.User;
using Blog.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Application
{
    public static class BlogApplicationExtension
    {
        public static void AddApplication(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(BlogApplicationExtension));

            services.AddTransient<IUserver, Userver>();

            services.AddTransient<IBlogServer, BlogServer>();

            services.AddTransient<IBlogTypeService, BlogTypeService>();

            services.AddEntityFrameWorkCore();
        }
    }
}
