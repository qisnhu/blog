﻿using AutoMapper;
using Blog.Application.Contract.Base;
using Blog.Application.Contract.Blogs;
using Blog.Application.Contract.Blogs.Dto;
using Blog.Application.Contract.User.Dto;
using Blog.EntityFrameworkCore;
using Blog.Module;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Application.Blogs
{
    public class BlogServer : IBlogServer
    {
        private readonly BlogDbContext dbContext;

        private readonly IMapper mapper;
        private readonly CurrentService currentService;
        public BlogServer(BlogDbContext dbContext, IMapper mapper, CurrentService currentService)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
            this.currentService = currentService;
        }

        public async Task AddPageViewAsync(Guid blogId)
        {
            var data = await dbContext.Blogs.FirstOrDefaultAsync(x => x.Id == blogId);
            if (data != null)
            {
                data.PageView++;

                dbContext.Blogs.Update(data);

                await dbContext.SaveChangesAsync();
            }
        }

        public async Task CreatAsync(CreateBlogDto createBlogDto)
        {
            //拿到当前登录的用户Id
            var userId = currentService.GetUserId();

            if (!await dbContext.BlogTypes.AnyAsync(x => x.Id == createBlogDto.TypeId)){
                throw new BusiesExceptions("文章类型不存在");
            }

            var date = mapper.Map<Module.Blogs>(createBlogDto);
            date.AuthorId = userId;
            //添加到数据库
            await dbContext.Blogs.AddAsync(date);
            await dbContext.SaveChangesAsync();
        }

        public async Task CreateCommentAsync(CreateCommentDto input)
        {
            if (!await dbContext.Blogs.AnyAsync(x => x.Id == input.BlogId))
            {
                throw new BusiesExceptions("博客不存在");
            }

            // 获取当前用户id
            var userId = currentService.GetUserId();

            var data = mapper.Map<BlogComment>(input);
            data.UseId = userId; // 设置评论人
            data.CreateTime = DateTime.Now; // 评论时间

            // 添加博客评论
            await dbContext.BlogComments.AddAsync(data);

            await dbContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            //获取当前登录的Id
            var userId= currentService.GetUserId();

            //查找博客Id相同和作者必须是登录用户的Id的博客
            var date = await dbContext.Blogs.FirstOrDefaultAsync(x => x.Id == id && x.AuthorId == id);

            if (date!=null)
            {
                dbContext.Blogs.Remove(date);
                await dbContext.SaveChangesAsync();
            }
        }

        public async Task DeleteCommentAsync(Guid id)
        {
            var userId = currentService.GetUserId();

            // 获取本人发送的评论
            var comment = await dbContext.BlogComments.FirstOrDefaultAsync(x => x.Id == id && x.UseId == userId);

            if (comment == null)
            {
                return;
            }

            // 删除评论
            dbContext.BlogComments.Remove(comment);

            await dbContext.SaveChangesAsync();
        }

        public async Task<BlogDto> GetAsync(Guid id)
        {
            //未使用拆分
            //var data = await dbContext.Blogs.Where(x => x.Id == id).Include(x => x.AuthorId).Include(x => x.TypeId).FirstOrDefaultAsync();

            //使用AsSplitQuery 拆分之后的,避免笛卡尔积爆炸，笛卡尔积爆炸就是一个博客有作者，类型，同时拿到博客，作者，类型的信息
            //两个include必须使用拆分
            var data = await dbContext.Blogs.Where(x=>x.Id==id)
                .AsNoTracking()
                .AsSplitQuery()//拆分
                .Include(x=>x.User)
                .FirstOrDefaultAsync();

            if (data == null)
            {
                throw new BusiesExceptions("博客不存在", 404);

            }
            var dto = mapper.Map<BlogDto>(data);

            //获取博客点赞数量
            dto.Likes = await dbContext.BlogLikes.LongCountAsync(x => x.BlogId == id);

            //获取博客评论
            var comment = await dbContext.BlogComments.Where(x => x.BlogId == id).OrderByDescending(x => x.CreateTime).ToListAsync();

            dto.BlogComments = mapper.Map<List<BlogCommentsDto>>(dto);

            return dto;
        }

        public async Task<PageResponeDto<PageBlogDto>> GetBlogListAsync(BlogInput input)
        {
            var date = dbContext.Blogs.AsQueryable();

            //input.TypeId.HasValue判断 判断可空类型是否有除了null以外的值,返回结果为bool,有则返回true
            if (input.TypeId.HasValue)
            {
                date = date.Where(x=>x.Id == input.TypeId);

            }
            if (!string.IsNullOrEmpty(input.Keyword))
            {
                date =date.Where(x=>x.Title.Contains(input.Keyword)||x.Content.Contains(input.Keyword));
            }

            date = date.Include(x => x.BlogType).Include(x => x.User)
                .OrderBy(x => x.PageView).OrderByDescending(x => x.CreateTime);

            var result = await date.Skip(input.SkipCount).Take(input.MaxResultCount).ToListAsync();

            var count = await date.CountAsync();

            var likes= await dbContext.BlogLikes.Where(x=>result.Select(s=>s.Id)
            .Contains(x.BlogId)).Select(x=>x.BlogId).ToListAsync();

            var dto = mapper.Map<List<PageBlogDto>>(date);

            foreach(var d in dto)
            {
                d.Like = likes.Count(x => x == d.Id);
            }

            return new PageResponeDto<PageBlogDto>(count, dto);

        }

        public async Task LikeAsync(Guid id)
        {
            var userId = currentService.GetUserId();
            var date = await dbContext.BlogLikes.FirstOrDefaultAsync(x => x.BlogId == id && x.UserId == userId);

            //是否点赞
            if (date == null)
            {
                date = new Module.BlogLikes
                {
                    UserId = userId,
                    BlogId = id,
                    CreateTime = DateTime.Now

                };

                await dbContext.BlogLikes.AddAsync(date);
            }
            else
            {
                dbContext.BlogLikes.Remove(date);
            }

            await dbContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(CreateBlogDto updateBlogDto)
        {
            var userId = currentService.GetUserId();

            var date = await dbContext.Blogs.FirstOrDefaultAsync(x => x.AuthorId == userId);
            //将 updateBlogDto 映射到date

            mapper.Map(updateBlogDto, date);

            if(date == null)
            {
                throw new BusiesExceptions("博客不存在");
            }
            //更新date
            dbContext.Blogs.Update(date);

            //保存date
            await dbContext.SaveChangesAsync();
        }
    }
}
