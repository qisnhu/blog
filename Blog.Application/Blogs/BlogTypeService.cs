﻿using AutoMapper;
using Blog.Application.Contract.Blogs;
using Blog.Application.Contract.Blogs.Dto;
using Blog.EntityFrameworkCore;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Application.Blogs
{
    public class BlogTypeService : IBlogTypeService
    {
        private readonly BlogDbContext _dbContext;

        private readonly IMapper mapper;
        public BlogTypeService(BlogDbContext dbContext,IMapper mapper)
        {
            _dbContext = dbContext;
            this.mapper = mapper;
        }

        public async Task CreateAsync(CreateBlogTypeDto createBlogTypeDto)
        {
            if(await _dbContext.BlogTypes.AnyAsync(x => x.Name == createBlogTypeDto.Name))
            {
                throw new BusiesExceptions("已经存在相同的博客类型");
            }
            var date = mapper.Map<Module.BlogType>(createBlogTypeDto);
            
            date.CreateTime= DateTime.Now;
            await _dbContext.BlogTypes.AddAsync(date);
            await _dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeletAsync(Guid id)
        {
            var type = await _dbContext.BlogTypes.FirstOrDefaultAsync(x => x.Id == id);

            if (type != null)
            {
                if(await _dbContext.Blogs.AnyAsync(x => x.TypeId == type.Id))
                {
                    throw new BusiesExceptions("该类型下面存在博客，不能删除");
                }
                type.IsDeleted = true;
                await _dbContext.SaveChangesAsync();
            }
        }

        public async Task<List<BlogTypeDto>> GetListAsync()
        {
            var data = await _dbContext.BlogTypes.ToListAsync();
            var dto = mapper.Map<List< BlogTypeDto>>(data);
            return dto;
        }

        public async Task UpdateAsync(UpdateBlogTypeDto input)
        {
            var data = await _dbContext.BlogTypes.FirstOrDefaultAsync(x => x.Id == input.Id);
            
            if (data == null)
            {
                throw new BusiesExceptions("博客类型不存在");
            }

            data.Name = input.Name;

            _dbContext.BlogTypes.Update(data);

            await _dbContext.SaveChangesAsync();
        }
    }
}
