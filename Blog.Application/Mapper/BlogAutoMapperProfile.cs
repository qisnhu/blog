﻿using AutoMapper;
using Blog.Application.Contract.Blogs.Dto;
using Blog.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Application.Mapper
{
    public class BlogAutoMapperProfile : Profile
    {
        public BlogAutoMapperProfile()
        {
            CreateMap<CreateBlogDto,Blog.Module.Blogs>();
            //相互映射
            //CreateMap<BlogDto, Module.Blogs>().ReverseMap(); 即
            //CreateMap<BlogDto, Module.Blogs>();和CreateMap<Module.Blogs, BlogDto>();
            CreateMap<BlogDto, Module.Blogs>().ReverseMap();
            CreateMap<CreateBlogTypeDto, Module.BlogType>();

            CreateMap<BlogTypeDto, Module.BlogType>().ReverseMap();

            CreateMap<CreateBlogDto, BlogComment>();

            CreateMap<UpdateBlogTypeDto,Module.BlogType>();

            CreateMap<Module.Blogs, PageBlogDto>();

            CreateMap<Module.BlogType, BlogTypeDto>();

            CreateMap<Module.BlogComment, BlogCommentsDto>();
        }

    }
}
