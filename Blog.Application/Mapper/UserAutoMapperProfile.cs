﻿using AutoMapper;
using Blog.Application.Blogs;
using Blog.Application.Contract.User.Dto;
using Blog.EntityFrameworkCore;
using Blog.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Application.Mapper
{
    public  class UserAutoMapperProfile:Profile
    {

       public UserAutoMapperProfile()
        {
            CreateMap<CreateUserDto, Module.User>();
            CreateMap<Module.User, UserDto>()
          .ForMember(x => x.Role, x => x.MapFrom(o => o.Role.ToString()));
            CreateMap<Module.User, BlogUserDto>();


        }


    }
}
