﻿using AutoMapper;
using Blog.Application.Contract.User;
using Blog.Application.Contract.User.Dto;
using Blog.EntityFrameworkCore;
using Blog.Module;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Application.User
{
    public class Userver : IUserver
    {
        private readonly BlogDbContext blogDbContext;
        private readonly IMapper mapper;
        private readonly CurrentService _currentService;


        public Userver(BlogDbContext blogDbContext, IMapper mapper)
        {
            this.blogDbContext = blogDbContext;
            this.mapper = mapper;
        }
        
        /// <summary>
        /// 添加用户
        /// </summary>
        /// <param name="userDto"></param>
        /// <returns></returns>
        /// <exception cref="BusiesExceptions"></exception>
        public async Task CreateUserAsync(CreateUserDto userDto)
        {
            if (await blogDbContext.Users.AnyAsync(x => x.UserName == userDto.UserName))
            {
                throw new BusiesExceptions("用户名已经存在");
            }

            //mapper映射
            var date = mapper.Map<Module.User>(userDto);

            date.CreateTime = DateTime.Now;
            //添加
            await blogDbContext.AddAsync(date);

            //保存
            await blogDbContext.SaveChangesAsync();

        }

        public async Task DeleteAsync(Guid id)
        {
            var user = await blogDbContext.Users.FirstOrDefaultAsync(x => x.Id == id);
            if (user != null)
            {
                blogDbContext.Users.Remove(user);
                await blogDbContext.SaveChangesAsync();
            }
        }

        public async Task<UserDto> LoginAsync(LoginDto loginDto)
        {
            var data = await blogDbContext.Users.FirstOrDefaultAsync
                (x => x.UserName == loginDto.UserName && x.Password == loginDto.Password);

            if(data == null)
            {
                throw new BusiesExceptions("账号密码错误");
            }
            var dto = mapper.Map<UserDto>(data);

            return dto;

        }

        public async Task UpdateAsync(UseupdateDto userDto)
        {
            var userId = _currentService.GetUserId();

            var result = await blogDbContext.Users.FirstOrDefaultAsync(x => x.Id == userId);

            if (result == null)
            {
                return;
            }

            mapper.Map(userDto, result);

            blogDbContext.Users.Update(result);

            await blogDbContext.SaveChangesAsync();
        }
    }
}
