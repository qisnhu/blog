﻿using Blog.Module;
using Blog.Module.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.EntityFrameworkCore
{
    public class BlogDbContext : DbContext
    {
        public DbSet<Blogs>  Blogs { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<BlogType> BlogTypes { get; set; }

        public DbSet<BlogComment> BlogComments { get; set; }

        public DbSet<BlogLikes> BlogLikes { get; set; }

        //public BlogDbContext()
        //{

        //}

        public BlogDbContext(DbContextOptions options) : base(options)
        {
        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //显示更详细的异常信息
            optionsBuilder.EnableDetailedErrors();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Blogs>(x =>
            {
                //表名
                x.ToTable("T_Blogs");

                //表描述
                x.HasComment("博客文章");

                //主键
                x.HasKey(x => x.Id);

                //索引
                x.HasIndex(x => x.Id);
                x.HasIndex(x => x.TypeId);

                x.HasIndex(x => x.AuthorId);

                x.HasIndex(x => x.Title);
            });

            modelBuilder.Entity<User>(x =>
            {
                x.ToTable("T_Users");
                x.HasComment("用户表");
                x.HasKey(x => x.Id);
                x.HasIndex(x => x.Id);
                //唯一索引
                x.HasIndex(x => x.UserName).IsUnique();
            });

            modelBuilder.Entity<BlogType>(x =>
            {
                x.ToTable("T_BlogTypes");
                x.HasComment("博客类型");
                x.HasKey(x => x.Id);
                x.HasIndex(x => x.Id);
                x.HasIndex(x => x.Name);

            });

            modelBuilder.Entity<BlogLikes>(x =>
            {
                x.ToTable("T_BlogLikes");
                x.HasComment("博客浏览量");
                x.HasKey(x => x.Id);
                x.HasIndex(x => x.Id);
            });

            modelBuilder.Entity<BlogComment>(x =>
            {
                x.ToTable("T_BlogComment");
                x.HasComment("博客评论");
                x.HasKey(x => x.Id);
                x.HasIndex(x => x.Id);
                x.HasIndex(x => x.UseId);
            });


        }
    }

}
