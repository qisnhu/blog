﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Blog.EntityFrameworkCore.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "T_BlogTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    CreateTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeleteTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_BlogTypes", x => x.Id);
                },
                comment: "博客类型");

            migrationBuilder.CreateTable(
                name: "T_Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Avata = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Role = table.Column<int>(type: "int", nullable: false),
                    CreateTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeleteTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_Users", x => x.Id);
                },
                comment: "用户表");

            migrationBuilder.CreateTable(
                name: "T_Blogs",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Content = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PageView = table.Column<long>(type: "bigint", nullable: false),
                    TypeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AuthorId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreateTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeleteTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_Blogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_T_Blogs_T_BlogTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "T_BlogTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_T_Blogs_T_Users_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "T_Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "博客文章");

            migrationBuilder.CreateTable(
                name: "T_BlogComment",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UseId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    BlogId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Content = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreateTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeleteTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_BlogComment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_T_BlogComment_T_Blogs_BlogId",
                        column: x => x.BlogId,
                        principalTable: "T_Blogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_T_BlogComment_T_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "T_Users",
                        principalColumn: "Id");
                },
                comment: "博客评论");

            migrationBuilder.CreateTable(
                name: "T_BlogLikes",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    BlogId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    BlogsId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreateTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeleteTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_BlogLikes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_T_BlogLikes_T_Blogs_BlogsId",
                        column: x => x.BlogsId,
                        principalTable: "T_Blogs",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_T_BlogLikes_T_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "T_Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "博客浏览量");

            migrationBuilder.CreateIndex(
                name: "IX_T_BlogComment_BlogId",
                table: "T_BlogComment",
                column: "BlogId");

            migrationBuilder.CreateIndex(
                name: "IX_T_BlogComment_Id",
                table: "T_BlogComment",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_T_BlogComment_UseId",
                table: "T_BlogComment",
                column: "UseId");

            migrationBuilder.CreateIndex(
                name: "IX_T_BlogComment_UserId",
                table: "T_BlogComment",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_T_BlogLikes_BlogsId",
                table: "T_BlogLikes",
                column: "BlogsId");

            migrationBuilder.CreateIndex(
                name: "IX_T_BlogLikes_Id",
                table: "T_BlogLikes",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_T_BlogLikes_UserId",
                table: "T_BlogLikes",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_T_Blogs_AuthorId",
                table: "T_Blogs",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_T_Blogs_Id",
                table: "T_Blogs",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_T_Blogs_Title",
                table: "T_Blogs",
                column: "Title");

            migrationBuilder.CreateIndex(
                name: "IX_T_Blogs_TypeId",
                table: "T_Blogs",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_T_BlogTypes_Id",
                table: "T_BlogTypes",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_T_BlogTypes_Name",
                table: "T_BlogTypes",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_T_Users_Id",
                table: "T_Users",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_T_Users_UserName",
                table: "T_Users",
                column: "UserName",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "T_BlogComment");

            migrationBuilder.DropTable(
                name: "T_BlogLikes");

            migrationBuilder.DropTable(
                name: "T_Blogs");

            migrationBuilder.DropTable(
                name: "T_BlogTypes");

            migrationBuilder.DropTable(
                name: "T_Users");
        }
    }
}
