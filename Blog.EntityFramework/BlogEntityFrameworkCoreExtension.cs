﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.EntityFrameworkCore
{
    public static class BlogEntityFrameworkCoreExtension
    {
        public static void AddEntityFrameWorkCore(this IServiceCollection services)
        {
            var configure = services.BuildServiceProvider().GetRequiredService<IConfiguration>();
            services.AddDbContext<BlogDbContext>(option => option.UseSqlServer(configure["ConnectionStrings:SqlserverConn"]));
        }
    }
}
