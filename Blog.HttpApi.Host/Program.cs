using Infrastructure;
using Blog.Application;
using Blog.HttpApi.Host.Filters;
using Blog.HttpApi.Host.Options;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerUI;
using System.Text;

var builder = WebApplication.CreateBuilder(args);
///Swagger配置


// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

//注入Appliaction的里面的服务
builder.Services.AddApplication();

//注入Infrastructure 里面的服务CurrentService里面的扩展方法CurrentExtension的AddCurrent
builder.Services.AddCurrent();

//builder.Services.AddSwaggerGen();
//SwaggerGen配置----开始
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1.0", new OpenApiInfo()
    {
        Version = "v1.0",
        Title = "博客Api",
        Description = "博客的WebApiSwagger文档",
        //Contact = new OpenApiContact
        //{
        //    Name = "Simple",
        //    Email = "239573049@qq.com",
        //    Url = new Uri("https://github.com/239573049")
        //}
    });

    string[] files = Directory.GetFiles(AppContext.BaseDirectory, "*.xml");//获取api文档
    string[] array = files;
    foreach (string filePath in array)
    {
        options.IncludeXmlComments(filePath, includeControllerXmlComments: true);
    }

    options.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Id = "Bearer",
                                Type = ReferenceType.SecurityScheme
                            }
                        },
                        Array.Empty<string>()
                    }
                });
    options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        Description = "填写你的Tokne 需要添加前缀 Bearer {{token}}",
        Name = "Authorization",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.ApiKey
    });
});

//SwaggerGen配置----结束


//过滤器
builder.Services.AddMvcCore(x =>
{
    x.Filters.Add<ExceptionFilter>();
    x.Filters.Add<ResponeFilter>();
});

//jwt配置---开始
//1.读取appsettings里的配置文件名为Jwt
IConfigurationSection congfigrautionSection = builder.Services.BuildServiceProvider().GetRequiredService<IConfiguration>().GetSection("Jwt");
//2.把读取的配置文件json变成 JwtOptions类
builder.Services.Configure<JwtOptions>(congfigrautionSection);
//3.获取JwtOptions类的内容
var jwt = congfigrautionSection.Get<JwtOptions>();
//4.配置jwt
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(option =>
    {

        option.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
        {
            ValidateIssuer = true, //是否在令牌期间验证签发者
            ValidateAudience = true, //是否验证接收者
            ValidateLifetime = true, //是否验证失效时间
            ValidateIssuerSigningKey = true, //是否验证签名
            ValidAudience = jwt.Audience, //接收者
            ValidIssuer = jwt.Issuer, //签发者，签发的Token的人
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwt.SecretKey!)) // 密钥
        };
    });
////jwt配置 ---结束
///



///文件 ---开始
IConfigurationSection fileConfiguration = builder.Services.BuildServiceProvider().GetRequiredService<IConfiguration>().GetSection("FileOption");
//注入
builder.Services.Configure<FileOption>(fileConfiguration);

///文件 ---结束











var app = builder.Build();

// Configure the HTTP request pipeline.
//配置 Swagger
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        c.SwaggerEndpoint("/swagger/v1.0/swagger.json", "博客Api");
        c.DocExpansion(DocExpansion.None);
        c.DefaultModelsExpandDepth(-1);
    });
}
//注册的顺序
app.UseAuthentication();
app.UseAuthorization();

app.UseStaticFiles();
app.MapControllers();

app.Run();
