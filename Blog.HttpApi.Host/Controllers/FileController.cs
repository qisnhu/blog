﻿using Blog.HttpApi.Host.Options;
using Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Blog.HttpApi.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        private readonly FileOption fileOption;

        public FileController(IOptions<FileOption> fileOption)
        {
            this.fileOption = fileOption.Value;
        }


        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<string> UploadFileAsync(IFormFile file)
        {
           if(file.Length > ((fileOption.Maxsize* 1024) * 1024))
            {
                throw new BusiesExceptions("文件超过限制大小");

            }
           //GUid转化为数字
           var name = Guid.NewGuid().ToString("N")+file.FileName;
            if (!Directory.Exists("./wwwroot/file"))
            {
                Directory.CreateDirectory("./wwwroot/file");
            }


            var fileStream = System.IO.File.Create(Path.Combine("./wwwroot/file", name));

            await file.OpenReadStream().CopyToAsync(fileStream);
            //释放文件流
            await file.OpenReadStream().DisposeAsync();

            fileStream.Close();

            return "file/" + name;


        }
    }
}
