﻿using Blog.Application.Contract.Blogs;
using Blog.Application.Contract.Blogs.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace Blog.HttpApi.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlogTypeController : ControllerBase
    {
        private readonly  IBlogTypeService blogTypeService;

        public BlogTypeController(IBlogTypeService blogTypeService)
        {
            this.blogTypeService = blogTypeService;
        }

        /// <summary>
        /// 创建博客类型
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async  Task CreateBlogTypeAsync(CreateBlogTypeDto createBlogTypeDto)
        {
            await blogTypeService.CreateAsync(createBlogTypeDto);
        }

        [HttpDelete]
        public async Task DeleteBlogTypeAsync(Guid id)
        {
            await blogTypeService.DeletAsync(id);
        }
        [HttpPut]
        public async  Task UpdateAsync(UpdateBlogTypeDto updateBlogTypeDto)
        {
            await blogTypeService.UpdateAsync(updateBlogTypeDto);
        }


        /// <summary>
        /// 获取博客所有类型
        /// </summary>
        /// <returns></returns>
        [HttpGet("list")]
        public async Task<List<BlogTypeDto>> GetListAsync()
        {
           return await blogTypeService.GetListAsync();
        }
    }
}
