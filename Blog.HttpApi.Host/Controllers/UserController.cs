﻿using Blog.Application.Contract.User;
using Blog.Application.Contract.User.Dto;
//using Blog.EntityFrameworkCore.Migrations;
using Blog.HttpApi.Host.Options;
using Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Blog.HttpApi.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserver userServer;
        private readonly JwtOptions _jwtOptions;

        public UserController(IUserver userServer, IOptions<JwtOptions> jwtOptions)
        {
            this.userServer = userServer;
            this._jwtOptions = jwtOptions.Value;

        }

        //注册
        [HttpPost]
        //[Authorize(Roles ="Admin")]
        public async Task CreateUserAsync(CreateUserDto userDto)
        {
            await userServer.CreateUserAsync(userDto);

        }

        [HttpPost("login")]
        public async Task<string> LoginAsync(LoginDto loginDto)
        {
            // 通过账号密码获取用户信息
            var user = await userServer.LoginAsync(loginDto);

            // 添加Claim
            var claims = new[]
            {
                new Claim(Constant.Id, user.Id.ToString()),
               new Claim(ClaimTypes.Role,user.Role) // 设置用户权限
          };

            // 加密
            var cred = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtOptions.SecretKey!)),
                SecurityAlgorithms.HmacSha256);
            var jwtSecurityToken = new JwtSecurityToken(
                _jwtOptions.Issuer, // 签发者
                _jwtOptions.Audience, // 接收者
                claims, // payload
                expires: DateTime.Now.AddMinutes(_jwtOptions.ExpireMinutes), // 过期时间
                signingCredentials: cred); // 令牌

            // 签发token
            var token = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);

            return token;
        }


    } 
}
