﻿using Blog.Application.Blogs;
using Blog.Application.Contract.Base;
using Blog.Application.Contract.Blogs;
using Blog.Application.Contract.Blogs.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace Blog.HttpApi.Host.Controllers
{
    /// <summary>
    /// 博客
    /// </summary>
    [Route("api/[controller]")]
    [Authorize(Roles = "Admin")]

    [ApiController]
    public class BlogsController : ControllerBase
    {
        private readonly IBlogServer blogServer;
        public BlogsController(IBlogServer blogServer)
        {
            this.blogServer = blogServer;
        }

        [HttpPost]

        ///创建博客
        public async Task CreateBlogAsync(CreateBlogDto createBlogDto)
        {
            await blogServer.CreatAsync(createBlogDto);
        }

        ///更新博客
        [HttpPut]
        ///
        public async Task UpdateBlogAsync(CreateBlogDto createBlogDto)
        {
            await blogServer.UpdateAsync(createBlogDto);
        }

        [HttpDelete("{id}")]
        public async Task DelteBlogAsync(Guid id)
        {
            await blogServer.DeleteAsync(id);
                
        }

        /// <summary>
        /// 获取博客详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [AllowAnonymous] //标记不需要权限
        public async Task<BlogDto> GetBlogAsync(Guid id)
        {
            return await blogServer.GetAsync(id);
        }

        /// <summary>
        /// 点赞博客
        /// </summary>
        /// <param name="id">博客Id</param>
        /// <returns></returns>
        [HttpGet("like/{id}")]
        public async Task LikeAsync(Guid id)
        {
            await blogServer.LikeAsync(id);
        }

        /// <summary>
        /// 添加评论
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("comment")]
        public async Task CreateCommentAsync(CreateCommentDto input)
        {
            await blogServer.CreateCommentAsync(input);
        }

        /// <summary>
        /// 删除评论
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("comment/{id}")]
        public async Task DeleteCommentAsync(Guid id)
        {
            await blogServer.DeleteCommentAsync(id);
        }

        /// <summary>
        /// 获取博客推荐类
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("list")]
        [AllowAnonymous] // 标记无权限
        public async Task<PageResponeDto<PageBlogDto>> GetListAsync([FromQuery] BlogInput input)
        {
            return await blogServer.GetBlogListAsync(input);
        }

    }
}
