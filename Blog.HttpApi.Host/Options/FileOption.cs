﻿namespace Blog.HttpApi.Host.Options
{
    public class FileOption
    {
        /// <summary>
        /// 限制文件大小 MB
        /// </summary>
        public int Maxsize { get; set; }

    }
}
