﻿using Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Blog.HttpApi.Host.Filters
{
    /// <summary>
    /// 异常过滤器
    /// </summary>
    public class ExceptionFilter:ExceptionFilterAttribute
    {
        private readonly ILogger<ExceptionFilter> logger;
        /// <inheritdoc/>

        public ExceptionFilter(ILogger<ExceptionFilter> logger)
        {
            this.logger = logger;
        }
     /// <inheritdoc/>

        public override void OnException(ExceptionContext context)
        {
            var cx = context.Exception;
            if (cx is BusiesExceptions busiesExceptions)
            {
                context.Result = new ObjectResult(new HttpResult(busiesExceptions.Code,
                    busiesExceptions.Message));

            }
            else
            {
                context.Result = new ObjectResult(new HttpResult(500,cx.Message));
            }

            context.ExceptionHandled = true;
        }
    }
}
