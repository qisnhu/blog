﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
   
    public class HttpResult
    {
        public HttpResult(int code)
        {
            Code = code;
        }
        public HttpResult(int code, string? message, object? datas)
        {
            Code = code;
            Message = message;
            Data = datas;
        }

        public HttpResult(int code, object? data)
        {
            this.Code = code;
            this.Data = data;
        }

        public HttpResult(int code, string? message)
        {
            Code = code;
            Message = message;
        }

        /// <summary>
        /// 状态码
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// 异常信息
        /// </summary>
        public string? Message { get; set; }

        /// <summary>
        /// 数据
        /// </summary>
        public object? Data { get; set; }


    }
}
