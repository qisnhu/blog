﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    /// <summary>
    /// 业务异常类
    /// </summary>
    public class BusiesExceptions:Exception
    {
        public int Code { get; }

        public BusiesExceptions(string?message,int code = 400) : base(message)
        {
            Code = code;
        }

    }
}
