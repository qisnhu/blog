﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Application.Contract.Base
{
    public class PageResponeDto<T>
    {
        public PageResponeDto(int total, IReadOnlyList<T> items)
        {
            Total = total;
            Items = items;
        }

        /// <summary>
        /// 总数
        /// </summary>
        public int Total { get; set; }

        public IReadOnlyList<T> Items { get; set; }


    }
}
