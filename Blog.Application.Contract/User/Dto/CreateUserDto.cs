﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Application.Contract.User.Dto
{
    public class CreateUserDto
    {
        // <summary>
        /// 用户名不可以空
        /// </summary>
        [MinLength(6,ErrorMessage ="用户名不能小于6位")]
        public string UserName { get; set; } = null!;


        /// <summary>
        /// 密码不可以为空
        /// </summary>
        [MaxLength(6, ErrorMessage = "密码不能小于6位")]

        public string Password { get; set; } = null!;

        /// <summary>
        /// 头像
        /// </summary>
        public string? Avata { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        //
        public string? Email { get; set; }
    }
}
