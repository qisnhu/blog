﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Application.Contract.User.Dto
{
    public class LoginDto
    {
        // <summary>
        /// 用户名不可以空
        /// </summary>
        public string UserName { get; set; } = null!;


        /// <summary>
        /// 密码不可以为空
        /// </summary>
        public string Password { get; set; } = null!;
    }
}
