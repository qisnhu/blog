﻿using Blog.Application.Contract.User.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Application.Contract.User
{
    public interface IUserver
    {
        Task CreateUserAsync(CreateUserDto userDto);

        Task<UserDto> LoginAsync(LoginDto loginDto);

        Task DeleteAsync(Guid id);

        Task UpdateAsync(UseupdateDto userDto);
    }
}
