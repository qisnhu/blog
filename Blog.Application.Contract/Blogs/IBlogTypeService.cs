﻿using Blog.Application.Contract.Blogs.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Application.Contract.Blogs
{
    public interface IBlogTypeService
    {
        Task CreateAsync(CreateBlogTypeDto createBlogTypeDto);

        Task DeletAsync(Guid id);


        /// <summary>
        /// 更新博客
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task UpdateAsync(UpdateBlogTypeDto input);


        /// <summary>
        /// 获取博客列表
        /// </summary>
        /// <returns></returns>
        Task<List<BlogTypeDto>> GetListAsync();



    }
}
