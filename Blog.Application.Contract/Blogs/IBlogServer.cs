﻿using Blog.Application.Contract.Base;
using Blog.Application.Contract.Blogs.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Application.Contract.Blogs
{
    public interface IBlogServer
    {
        /// <summary>
        /// 创建博客
        /// </summary>
        /// <returns></returns>
        Task CreatAsync(CreateBlogDto createBlogDto);


        /// <summary>
        /// 删除博客
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteAsync(Guid id);


        /// <summary>
        /// 更新博客
        /// </summary>
        /// <param name="updateBlogDto"></param>
        /// <returns></returns>
       Task UpdateAsync(CreateBlogDto updateBlogDto);


        /// <summary>
        /// 获取博客详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<BlogDto> GetAsync(Guid id);


        /// <summary>
        /// 博客点赞 
        /// 存在点赞将取消点赞
        /// 不存在即点赞
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task LikeAsync(Guid id);


        /// <summary>
        /// 添加博客评论
        /// </summary>
        /// <returns></returns>
        Task CreateCommentAsync(CreateCommentDto input);

        /// <summary>
        /// 删除评论
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteCommentAsync(Guid id);

        /// <summary>
        /// 获取博客推荐信息
        /// </summary>
        /// <returns></returns>
        //Task<PageResponseDto<PageBlogDto>> GetBlogListAsync(BlogInput input);

        /// <summary>
        /// 添加浏览量
        /// </summary>
        /// <param name="blogId"></param>
        /// <returns></returns>
        Task AddPageViewAsync(Guid blogId);


        /// <summary>
        /// 获取博客推荐信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PageResponeDto<PageBlogDto>> GetBlogListAsync(BlogInput input);


 


    }
}
