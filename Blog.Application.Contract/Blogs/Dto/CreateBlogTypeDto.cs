﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Application.Contract.Blogs.Dto
{
    public class CreateBlogTypeDto
    {
        [Required(ErrorMessage ="博客类型名不能为空")]
        [MinLength(2,ErrorMessage = "博客类型名不能小于2位")]
        public string Name { get; set; } = null!;

    }
}
