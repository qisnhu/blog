﻿using Blog.Application.Blogs;
using Blog.Application.Contract.Base;
using Blog.Application.Contract.User.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Application.Contract.Blogs.Dto
{
    public class BlogDto:EntityDto
    {
        /// <summary>
        /// 不允许为空 题目
        /// </summary>
        public string Title { get; set; } = null!;

        /// <summary>
        /// 内容 允许为空
        /// </summary>
        public string? Content { get; set; }
   
        /// <summary>
        /// 文章类型Id
        /// </summary>
        public Guid TypeId { get; set; }

        /// <summary>
        /// 作者Id
        /// </summary>

        public Guid AuthorId { get; set; }

        /// <summary>
        /// 点赞量
        /// </summary>
        
        public long Likes { get; set; }

        /// <summary>
        /// 博客类型
        /// </summary>
        public virtual BlogTypeDto? BlogType { get; set; }

        /// <summary>
        /// 导航属性 作者
        /// </summary>
        public virtual BlogUserDto? User { get; set; }


        /// <summary>
        /// 博客评论列表
        /// </summary>
        public virtual List<BlogCommentsDto>? BlogComments { get; set; }


    }
}
