﻿using Blog.Application.Contract.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Application.Contract.Blogs.Dto
{
    public class CreateBlogDto
    {
        /// <summary>
        /// 不允许为空 题目
        /// </summary>
        public string Title { get; set; } = null!;

        /// <summary>
        /// 内容 允许为空
        /// </summary>
        public string? Content { get; set; }

        /// <summary>
        /// 文章类型Id
        /// </summary>
        public Guid TypeId { get; set; }



       
    }
}
