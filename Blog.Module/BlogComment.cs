﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Blog.Module.Base;

namespace Blog.Module
{
    /// <summary>
    /// 博客评论
    /// </summary>
    public class BlogComment : Entity
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public Guid UseId { get; set; }

        /// <summary>
        /// 博客Id
        /// </summary>
        public Guid BlogId { get; set; }

        /// <summary>
        /// 评论内容
        /// </summary>
        public string? Content { get; set; }


        /// <summary>
        /// 导航属性 User
        /// </summary>
        public virtual User? User { get; set; }


        /// <summary>
        /// 导航属性博客
        /// </summary>
        [ForeignKey("BlogId")]

        public virtual Blogs? Blogs { get; set; }

    }
}
