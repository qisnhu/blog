﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Blog.Module.Base;

namespace Blog.Module
{
    /// <summary>
    /// 用户
    /// </summary>
    public class User : Entity
    {
        public User()
        {
            CreateTime= DateTime.Now;
        }
        /// <summary>
        /// 用户名不可以空
        /// </summary>
        public string UserName { get; set; } = null!;


        /// <summary>
        /// 密码不可以为空
        /// </summary>
        public string Password { get; set; } = null!;

        /// <summary>
        /// 头像
        /// </summary>
        public string? Avata { get; set; } 

        /// <summary>
        /// 邮箱
        /// </summary>
        public string? Email { get; set; }

        /// <summary>
        /// 角色
        /// </summary>
        public RoleType Role { get; set; }
    }
}
