﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Blog.Module.Base;

namespace Blog.Module
{
    /// <summary>
    /// 博客文章
    /// </summary>
    public class Blogs : Entity
    {
        /// <summary>
        /// 不允许为空 题目
        /// </summary>
        public string Title { get; set; } = null!;

        /// <summary>
        /// 内容 允许为空
        /// </summary>
        public string? Content { get; set; }
        /// <summary>
        /// 浏览量 
        /// </summary>
        public long PageView { get; set; }

        /// <summary>
        /// 文章类型Id
        /// </summary>
        public Guid TypeId { get; set; }

        /// <summary>
        /// 作者Id
        /// </summary>
        public Guid AuthorId { get; set; }

        /// <summary>
        /// 博客类型
        /// </summary>
        [ForeignKey("TypeId")]

        public virtual BlogType? BlogType { get; set; }

        /// <summary>
        /// 导航属性 作者
        /// </summary>
        [ForeignKey("AuthorId")]
        public virtual User? User { get; set; }

        /// <summary>
        /// 点赞人数
        /// </summary>
        /// <summary>
        /// 点赞人员
        /// </summary>
        public virtual List<BlogLikes> BlogLikes { get; set; }





        public Blogs()
        {
            BlogLikes = new List<BlogLikes>();

        }
    }
}
