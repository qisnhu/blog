﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Blog.Module.Base;

namespace Blog.Module
{
    public class BlogLikes : Entity
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// 博客Id
        /// </summary>
        public Guid BlogId { get; set; }

        /// <summary>
        /// 博客
        /// </summary>
        public Blogs? Blogs { get; set; }

        /// <summary>
        /// 用户
        /// </summary>
        public User? User { get; set; }



    }
}
